package com.javariga6.homework.controler;

import com.javariga6.homework.enums.Status;
import com.javariga6.homework.model.Order;
import com.javariga6.homework.model.Product;
import com.javariga6.homework.repository.OrderRepository;
import com.javariga6.homework.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("orders")
public class OrderController {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;

    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @GetMapping("")
    public String index(Order order, Model model){
        model.addAttribute("orders", orderRepository.findAll());
        return "order-list";
    }

    @GetMapping("add")
    public String addOrderForm(Order order){
        return "add-order";
    }

    @PostMapping("/addorder")
    public String addUser(Order order, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-order";
        }
        order.setStatus(Status.CREATED);
        orderRepository.save(order);
        model.addAttribute("order", order);
        model.addAttribute("products", productRepository.findAll());
        return "add-product-to-order";
    }
    @GetMapping("/addOrder/{orderid}")
    public String addToCustomOrder(@PathVariable Long orderid, Model model){
        List<Product> availableProducts = productRepository.findAll();
        Order order = orderRepository.findById(orderid).get();
        order.getProducts().forEach(availableProducts::remove);
        model.addAttribute("order", order);
        model.addAttribute("products", availableProducts);
        return "add-product-to-order";
    }

    @GetMapping("/addproduct/{orderid}/{productid}")
    public String addProduct(@PathVariable Long orderid, @PathVariable Long productid, Model model){
        Order order = orderRepository.getOne(orderid);
        order.getProducts().add(
                productRepository.getOne(productid)
        );
        orderRepository.save(order);
        List<Product> availableProducts = productRepository.findAll();
        order.getProducts().forEach(availableProducts::remove);
        model.addAttribute("order", order);
        model.addAttribute("products", availableProducts);
        return "add-product-to-order";
    }

    @PostMapping("/findOrderById")
    public String findById(Order order, BindingResult result, Model model) {
        if (orderRepository.findById(order.getId()).isPresent()) {
            model.addAttribute("orders", orderRepository.findById(order.getId()).get());
        }
        return "order-list";
    }
    @PostMapping("/findOrderByName")
    public String findByClientId(Order order, BindingResult result, Model model) {
        if (!orderRepository.findByClientId(order.getClientId()).isEmpty()) {
            model.addAttribute("orders", orderRepository.findByClientId(order.getClientId()));
        }
        return "order-list";
    }

}
