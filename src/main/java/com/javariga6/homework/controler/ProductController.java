package com.javariga6.homework.controler;

import com.javariga6.homework.model.Product;
import com.javariga6.homework.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("products")
public class ProductController {
    ProductRepository productRepository;

    @Autowired
    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping("")
    public String index(Product product, Model model){
        model.addAttribute("products", productRepository.findAll());
        return "product-list";
    }
    @GetMapping("add")
    public String addProductForm(Product product){
        return "add-product";
    }

    @PostMapping("/addproduct")
    public String addProduct(Product product, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-product";
        }
        productRepository.save(product);
        return "redirect:/products";
    }

    @PostMapping("/findProductById")
    public String findById(Product product, BindingResult result, Model model) {
        if (productRepository.findById(product.getId()).isPresent()) {
            model.addAttribute("products", productRepository.findById(product.getId()).get());
        }
        return "product-list";
    }
    @PostMapping("/findProductByName")
    public String findByName(Product product, BindingResult result, Model model) {
        if (!productRepository.findByName(product.getName()).isEmpty()) {
            model.addAttribute("products", productRepository.findByName(product.getName()));
        }
        return "product-list";
    }

}
