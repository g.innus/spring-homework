package com.javariga6.homework.enums;

public enum Status {
    CREATED,
    ACCEPTED,
    COMPLETED,
}
